export class Axes{
    
    constructor(scene, THREE)
    {
        this.scene = scene;
        this.THREE = THREE;
        this.object = this.generateButtonForAxes();
    }

    generateButtonForAxes()
    {
        const axesHelper = new this.THREE.AxesHelper( 75 );
        return axesHelper;
    }

    update()
    {

    }
}




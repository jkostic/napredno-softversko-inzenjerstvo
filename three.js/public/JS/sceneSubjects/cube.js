
export class Cube{

    constructor(scene, THREE, dimension, x, y, z){
        this.scene = scene;
        this.THREE = THREE;
        this.object = this.generateCube(dimension, x, y, z);
    }

    generateCube(dimension, x, y, z){
        const geometry = new this.THREE.BoxGeometry( dimension, dimension, dimension );
        const material = new this.THREE.MeshBasicMaterial( {color: 0xff0000} );
        const cube = new this.THREE.Mesh( geometry, material );
        cube.position.x = x;
        cube.position.y = y;
        cube.position.z = z;
        cube.name = "cube";
        this.scene.add( cube );
        
        return cube;
    }

    update()
    {
    }

}


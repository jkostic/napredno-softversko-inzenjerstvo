
export class Cone{

    constructor(scene, THREE, radius, height, x, y, z){
        this.scene = scene;
        this.THREE = THREE;
        this.object = this.generateCone(radius, height, x, y, z);
        this.object.castShadow=true;
        
    }

    generateCone(radius, height, x, y, z){
        const geometry = new this.THREE.ConeGeometry(radius, height);
        const material = new this.THREE.MeshPhongMaterial( {color: 0x0000ff} );
        const cone = new this.THREE.Mesh( geometry, material );  
        cone.position.x = x;
        cone.position.y = y;
        cone.position.z = z;
        cone.name="cone";
        this.scene.add( cone );
        
        return cone;
    }

    update()
    {}
}




export class Text
{
    constructor(scene,THREE)
    {
        this.scene=scene;
        this.THREE=THREE;
        this.object;
        this.generateText();

    }
  
    generateText()
    {
        var loader = new this.THREE.FontLoader();

        loader.load( 'FONTS/Modern LCD-7_Regular.json', (font) =>
        {
        
            var geometry = new this.THREE.TextGeometry('Napredno softversko inzenjerstvo', 
            {
                font: font,
                size: 2,
                height: 2
            });

            var material= new this.THREE.MeshBasicMaterial({color: 0x00bb00});
            this.object = new this.THREE.Mesh(geometry,material);
            this.object.position.x=-50;
            this.object.position.z=37;
            this.object.rotation.y=Math.PI/2;

            this.scene.add(this.object);
               
        });
        
    }

    update()
    {}
}



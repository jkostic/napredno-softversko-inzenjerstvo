export class Plane
{
 constructor(scene,THREE)
 {
     this.scene=scene;
     this.THREE=THREE;
     this.loader = new this.THREE.TextureLoader();
     this.object=this.generatePlane();
 }

 generatePlane()
 {
     const planeTexture = this.loader.load( 'IMG/planeTex.jpeg' );
     planeTexture.wrapS = planeTexture.wrapT = this.THREE.RepeatWrapping;
     planeTexture.repeat.set( 25, 25 );
     planeTexture.encoding = this.THREE.sRGBEncoding;

     const planeMaterial = new this.THREE.MeshPhongMaterial( { map: planeTexture } );

     let plane = new this.THREE.Mesh( new this.THREE.PlaneGeometry( 150, 150 ), planeMaterial );
     plane.position.y = 0;
     plane.rotation.x = - Math.PI / 2;
     plane.receiveShadow=true;
     this.scene.add( plane );

     return plane;
 }

 update()
 {
     
 }
}


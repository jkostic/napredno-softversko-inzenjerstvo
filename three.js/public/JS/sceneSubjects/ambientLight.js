export class AmbientLight{

    constructor(scene,THREE)
    {
        this.scene=scene;
        this.THREE=THREE;
        this.object=this.generateAmbientLight();

    }
    
    generateAmbientLight()
    {
        const ambientLight = new this.THREE.AmbientLight( 0xffffff);
        this.scene.add( ambientLight );
        return ambientLight;
    }

    update()
    {
       
    }
}


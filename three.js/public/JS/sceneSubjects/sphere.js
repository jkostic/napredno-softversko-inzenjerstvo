export class Sphere
{
    constructor(scene, THREE, TWEEN, dimension, x, y, z)
    {
        this.scene = scene;
        this.THREE = THREE;
        this.TWEEN = TWEEN;
        this.loader = new this.THREE.TextureLoader();
        this.object = this.generateSphere(dimension, x, y, z);
    }

    generateSphere(dimension, x, y, z)
    {
        const sphereTexture = this.loader.load( 'IMG/sphereTex.jpg' );
        sphereTexture.encoding = this.THREE.sRGBEncoding;

        const sphereMaterial = new this.THREE.MeshBasicMaterial( { map: sphereTexture } );

        const geometry = new this.THREE.SphereGeometry( dimension, 32, 32 );
        const sphere = new this.THREE.Mesh( geometry, sphereMaterial );
        sphere.position.x = x;
        sphere.position.y = y;
        sphere.position.z = z;
        this.scene.add( sphere );
        this.setTween(sphere);
        return sphere;
    }
    
    setTween(sphere)
    {
        var spherePosition =  new this.THREE.Vector3(-5, 4, -75);
        var sphereTarget = new this.THREE.Vector3(-5, 4, 75);
        var tweenSphere = new this.TWEEN.Tween(spherePosition)
                                .to(sphereTarget, 5000)
                                .easing( this.TWEEN.Easing.Back.InOut )
                                .onUpdate(function(){
                                        sphere.position.x = spherePosition.x;
                                        sphere.position.y = spherePosition.y;
                                        sphere.position.z = spherePosition.z;
                                    })
                                .onComplete(function(){
                                    sphereTarget.set(-5, 4, -75);
                                    sphereTween2.start();
                                })
                                .start();

        var sphereTween2 = new this.TWEEN.Tween(spherePosition)
                                        .to(sphereTarget, 5000)
                                        .easing(this.TWEEN.Easing.Back.InOut)
                                        .onUpdate(function(){
                                            sphere.position.x = spherePosition.x;
                                            sphere.position.y = spherePosition.y;
                                            sphere.position.z = spherePosition.z;
                                        })
                                        .onComplete(function(){
                                            sphereTarget.set(-5, 4, 75);
                                            tweenSphere.start();
                                        });
    }

    update()
    {
        this.TWEEN.update();
    }
}




import {OBJLoader} from '/jsm/loaders/OBJLoader.js';
export class ObjObject{
    constructor(scene,THREE)
    {
        this.scene=scene;
        this.THREE=THREE;
        this.object;
        this.generateObject();
    }

    generateObject()
    {   
        const loader = new OBJLoader();
        
        loader.load('OBJ/teamugobj.obj',
            
             (object )=> {
                object.position.x=5;
                object.position.z=5;
                object.traverse((child)=>
                {
                    if(child instanceof this.THREE.Mesh)
                    {
                        child.material.color.setHex(0xff00ff);
                    }
                })
                this.scene.add( object );
                this.object=object;
            },
            
            function ( xhr ) {
                console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
            },
            
            function ( error ) {
                console.log( 'An error happened' + error);
            }
        );

    }

    update()
    {}
}



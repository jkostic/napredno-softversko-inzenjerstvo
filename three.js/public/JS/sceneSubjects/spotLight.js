export class SpotLight{

    constructor(scene,THREE)
    {
        this.scene=scene;
        this.THREE=THREE;
        this.object=this.generateSpotLight();

    }

    generateSpotLight()
    {
        const spotLight = new this.THREE.SpotLight( 0xffffff );
        spotLight.position.set( 15 , 15, 0 );

        spotLight.castShadow = true;

        spotLight.shadow.mapSize.width = 1024;
        spotLight.shadow.mapSize.height = 1024;

        spotLight.shadow.camera.near = 0.1;
        spotLight.shadow.camera.far =100;
        spotLight.shadow.camera.fov = 75;
        spotLight.target.position.set(2.5,-15,0);
        
        this.scene.add( spotLight );
        this.scene.add(spotLight.target);
        return spotLight;
    }

    update()
    {}
}



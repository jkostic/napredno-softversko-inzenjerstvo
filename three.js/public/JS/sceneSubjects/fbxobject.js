import {FBXLoader} from '/jsm/loaders/FBXLoader.js';
export class FbxObject{
    constructor(scene, THREE, clock,camera)
    {
        this.scene = scene;
        this.THREE = THREE;
        this.object;
        this.mixer;
        this.objectReady = false;
        this.clock = clock;
        this.camera=camera;
        this.model;
        this.generateFbxObject();
    }

    generateFbxObject()
    {
        const loader = new FBXLoader();
        loader.load("OBJ/vanguard.fbx", model => {      
            const clips = model.animations; 
            console.log(clips);
            const mixer = new this.THREE.AnimationMixer(model);                         
            const clip = this.THREE.AnimationClip.findByName( clips,'mixamo.com');
            const action = mixer.clipAction( clip );
            action.timeScale = 100;
            action.play();
            model.scale.multiplyScalar(0.05); 
            model.position.x = -10;
            this.scene.add(model);
            this.model=model;
            this.object = {model, mixer};
            this.mixer = mixer;
            this.objectReady = true;

        });
    }

    update()
    {
        if(this.objectReady)
        {
            this.mixer.update(this.clock.getDelta());
            const vector= new this.THREE.Vector3(this.camera.position.x,this.camera.position.y,this.camera.position.z);
            this.model.lookAt(vector);
        }
    }
}



import { Cube } from './sceneSubjects/cube.js';
import { Cone } from './sceneSubjects/cone.js';
import * as THREE from '/build/three.module.js';
import {OrbitControls} from '/jsm/controls/OrbitControls.js';
import { SpotLight } from './sceneSubjects/spotLight.js';
import { AmbientLight } from './sceneSubjects/ambientLight.js';
import { Plane } from './sceneSubjects/plane.js';
import { Sphere } from './sceneSubjects/sphere.js';
import { Text } from './sceneSubjects/text.js';
import TWEEN from 'https://cdn.jsdelivr.net/npm/@tweenjs/tween.js@18.5.0/dist/tween.esm.js'
import { Axes } from './sceneSubjects/axes.js';
import { ObjObject } from './sceneSubjects/objObject.js';
import { FbxObject } from './sceneSubjects/fbxobject.js';

export class SceneManager{
    constructor(canvas) {
        this.canvas = canvas;
        this.clock = new THREE.Clock();
        this.screenDimensions = {
            width: canvas.width,
            height: canvas.height
        }
        this.scene = this.buildScene();
        this.renderer = this.buildRender(this.screenDimensions);
        this.camera = this.buildCamera(this.screenDimensions);
        this.sceneSubjects = this.createSceneSubjects(this.scene);
        this.controls = this.buildControls();
        
        this.axes = new Axes(this.scene, THREE);

        this.axesBtnClicked = false;
        this.cameraBtnClicked = false;

        this.raycaster = new THREE.Raycaster();
        this.mouse = new THREE.Vector2();
    }

    onDocumentMouseDown(event) {
        this.mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
        this.mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
        this.raycaster.setFromCamera( this.mouse, this.camera );
        var intersects = this.raycaster.intersectObjects( this.scene.children);
        for(var i = 0; i<intersects.length; i++)
        {
            if(intersects[i].object.name == "cube")
            {
                alert("Napredno softversko inzenjerstvo");
            }
        }
    }


    buildScene(){
        const scene = new THREE.Scene();
        scene.background = new THREE.Color("#000000");
        return scene;
    }

    buildRender({width, height}){
        const renderer = new THREE.WebGLRenderer({ canvas: canvas, antialias: true, alpha: true });
        renderer.setSize(width, height);
        renderer.shadowMap.enabled=true;
        return renderer;
    }

    buildCamera({width, height}){
        const aspectRation = width/height;
        const fieldOfView = 75;
        const nearPlane = 0.1;
        const farPlane = 100;
        const camera = new THREE.PerspectiveCamera(fieldOfView, aspectRation, nearPlane, farPlane);
        camera.position.z = 25;
        camera.position.y = 7;
        return camera;
    }

    buildControls(){
        const controls = new OrbitControls(this.camera, this.renderer.domElement);
        let cone=this.scene.getObjectByName("cone");
        const vec=new THREE.Vector3(cone.position.x,cone.position.y,cone.position.z);
        controls.target=vec;
        controls.enablePan = false;
        return controls;
    }

    createSceneSubjects(scene){
        const sceneSubjects = [
            new Cube(scene, THREE, 5, 0, 2.5, 0), 
            new Cone(scene,THREE,2.5,5,0,7.5,0), 
            new SpotLight(scene,THREE), new AmbientLight(scene,THREE),
            new Plane(scene,THREE),
            new Sphere(scene, THREE, TWEEN, 2, -5, 4, -75), 
            new Text(scene,THREE), 
            new ObjObject(scene,THREE),
            new FbxObject(scene, THREE, this.clock,this.camera)
        ];

        return sceneSubjects;
    }

    update(){
        const elapsedTime = this.clock.getElapsedTime();
        this.sceneSubjects.forEach(sceneSubject => {
            sceneSubject.update();
        });
        this.controls.update();
        this.renderer.render(this.scene, this.camera);
    }

    onWindowResize(){
        const {width, height} = canvas;

        this.screenDimensions.width = width;
        this.screenDimensions.height = height;

        this.camera.aspect = width/height;
        this.camera.updateProjectionMatrix();

        this.renderer.setSize(width, height);
        this.renderer.render(this.scene, this.camera);
    }

    axesClicked()
    {
        if(!this.axesBtnClicked)
        {
            this.scene.add(this.axes.object);
        }
        else
        {
            this.scene.remove(this.axes.object);
        }
        
        this.axesBtnClicked = !this.axesBtnClicked;
    }


    cameraClicked()
    {
        if(!this.cameraBtnClicked)
        {
            this.controls.autoRotate=true;
        }
        else
        {
            this.controls.autoRotate=false;
       }
        this.cameraBtnClicked = !this.cameraBtnClicked;
    }
}


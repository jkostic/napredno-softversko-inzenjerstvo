import {SceneManager} from './SceneManager.js';

const canvas = document.getElementById("canvas");
const axesBtn = document.getElementById("axes");
const cameraBtn=document.getElementById("camera");
const sceneManager = new SceneManager(canvas);

bindEventListeners();
render();

function bindEventListeners(){
    window.onresize = resizeCanvas;
	resizeCanvas();
    axesBtn.addEventListener("click", function(){
        axesClicked();
    });
    cameraBtn.addEventListener("click", function(){
        cameraClicked();
    });
    document.addEventListener("click", function(event){
        documentClicked(event);
    });
}

function documentClicked(event)
{
    sceneManager.onDocumentMouseDown(event);
}

function resizeCanvas(){
    canvas.style.width = '100%';
	canvas.style.height= '100%';
    
    canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;

    sceneManager.onWindowResize();
}

function axesClicked()
{
    sceneManager.axesClicked();
}

function cameraClicked()
{
    sceneManager.cameraClicked();
}

function render(){
    requestAnimationFrame(render);
    sceneManager.update();
}